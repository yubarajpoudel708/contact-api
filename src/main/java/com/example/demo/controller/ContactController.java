package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.models.Contacts;
import com.example.demo.repository.ContactRepository;

import javax.validation.Valid;
import java.util.List;

//@RestController annotation is a combination of Spring’s @Controller and @ResponseBody annotations.

@RestController
@RequestMapping("/api")
public class ContactController {
	@Autowired
    ContactRepository contactRepository;

    // Get All Contacts
	@GetMapping("/contact")
	public List<Contacts> getAllNotes() {
	    return contactRepository.findAll();
	}
	
    // Create a new Contact
	@PostMapping("/contact")
	public Contacts createContact(@Valid @RequestBody Contacts contact) {
		System.out.println("phone number = " + contact.getPhone_number());
	    return contactRepository.save(contact);
	}
	
    // Get a Single Contact
	
	@GetMapping("/contact/{id}")
	public Contacts getContactById(@PathVariable(value = "id") Long contactId) {
	    return contactRepository.findById(contactId)
	            .orElseThrow(() -> new ResourceNotFoundException("Contact", "id", contactId));
	}

    // Update a Contact
	
	@PutMapping("/contact/{id}")
	public Contacts updateContact(@PathVariable(value = "id") Long contactId,
	                                        @Valid @RequestBody Contacts contactDetails) {

	    Contacts contact = contactRepository.findById(contactId)
	            .orElseThrow(() -> new ResourceNotFoundException("Contact", "id", contactId));

	    contact.setName(contactDetails.getName());
	    contact.setPhone_number(contactDetails.getPhone_number());

	    Contacts updatedContact = contactRepository.save(contact);
	    return updatedContact;
	}

    // Delete a Contact
	@DeleteMapping("/contact/{id}")
	public ResponseEntity<?> deleteContact(@PathVariable(value = "id") Long contactId) {
	    Contacts contact = contactRepository.findById(contactId)
	            .orElseThrow(() -> new ResourceNotFoundException("Contact", "id", contactId));

	    contactRepository.delete(contact);
	    return ResponseEntity.ok().build();
	}
}
